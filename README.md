# CHVote: Common Frontend Theme [![pipeline status](https://gitlab.com/chvote2/shared-libraries/chvote-common-theme/badges/master/pipeline.svg)](https://gitlab.com/chvote2/shared-libraries/chvote-common-theme/commits/master)

This npm module holds the common angular material theme and CSS rules used by the CHVote project.

# Building

## Pre-requisites

- NodeJS 8 or later
- NPM

## Usage

To setup the environment do: `npm install`.

# Contributing
See [CONTRIBUTING.md](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/CONTRIBUTING.md)

# License
This application is Open Source software released under the [Affero General Public License 3.0](LICENSE) 
license.
